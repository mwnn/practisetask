﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tasks
{    
     public class Polynomial
    {
        private int[] _coefficients;
        private char _variableName;
        private int _presetDegree;
        private int _highestDegree;

        public int PresetDegree  => _presetDegree;

        public int HighestDegree => _highestDegree;


        public int this[int index]
        {
            get
            {
                if((index < 0) || (index >= _coefficients.Length))
                {
                    throw new ArgumentOutOfRangeException("Index mustn't be negative or more than current vector length.");                                
                }
                return _coefficients[index];
            }
             set
            {
                if ((index < 0) || (index >= _coefficients.Length))
                {                  
                   throw new ArgumentOutOfRangeException("Index mustn't be negative or more than current vector length.");                                  
                }
                _coefficients[index] = value;
            }
        }

        public static Polynomial Add(Polynomial polynomA, Polynomial polynomB)
        {
            int resultDegree = Math.Max(polynomA.PresetDegree, polynomB.PresetDegree);    
            int degreeDifference = Math.Abs(polynomA.PresetDegree - polynomB.PresetDegree);
 
            var resultPolynom = new Polynomial(resultDegree, new int[resultDegree + 1]);
            
            resultPolynom._highestDegree = (polynomA.PresetDegree > polynomB.PresetDegree) ? 
                polynomA.HighestDegree : polynomB.HighestDegree;
          
            var j = 0;

            for(var i = 0; i < resultDegree + 1; ++i)
            {                
                if(i >= degreeDifference)
                {
                     resultPolynom[i] = (polynomA.PresetDegree > polynomB.PresetDegree) ?
                     (polynomA[i] + polynomB[j]) : (polynomA[j] + polynomB[i]);
                     j++;
                }
                else
                {
                    resultPolynom[i] = (polynomA.PresetDegree > polynomB.PresetDegree) ? 
                    polynomA[i] : polynomB[i];
                }
            }            
            
          return resultPolynom;
        }

         public static Polynomial Subtract(Polynomial polynomA, Polynomial polynomB)
        {
            int resultDegree = Math.Max(polynomA.PresetDegree, polynomB.PresetDegree);    
            int degreeDifference = Math.Abs(polynomA.PresetDegree - polynomB.PresetDegree);
 
            var resultPolynom = new Polynomial(resultDegree, new int[resultDegree + 1]);  

             resultPolynom._highestDegree = (polynomA.PresetDegree > polynomB.PresetDegree) ? 
                polynomA.HighestDegree : polynomB.HighestDegree;

            var j = 0;

            for(var i = 0; i < resultDegree + 1; ++i)
            {
               if(i >= degreeDifference)
                {
                     resultPolynom[i] = (polynomA.PresetDegree > polynomB.PresetDegree) ?
                     (polynomA[i] - polynomB[j]) : (polynomA[j] - polynomB[i]);
                     j++;
                }
                else
                {
                    resultPolynom[i] = (polynomA.PresetDegree > polynomB.PresetDegree) ? 
                    polynomA[i] : -polynomB[i];
                }
            }
              return resultPolynom;
        }

         public static Polynomial Multiply(Polynomial polynomA, Polynomial polynomB)
        {
            int LengthOfExpandedPolynomForm = (polynomA.PresetDegree + 1) * (polynomB.PresetDegree + 1);
        
            var amountOfDegrees = new int[LengthOfExpandedPolynomForm];
            var expandedPolynomForm = new int[LengthOfExpandedPolynomForm];

            int polynomAtempDegree = polynomA.PresetDegree;
            int polynomBtempDegree = polynomB.PresetDegree;
            
            var k = 0;
          
            for(var i = 0; i < polynomA.PresetDegree + 1; i++)
            {
                for(var j = 0; j < polynomB.PresetDegree + 1; j++)
                {
                    expandedPolynomForm[k] = polynomA[i] * polynomB[j];
                   
                    if(expandedPolynomForm[k] != 0)
                    {
                        amountOfDegrees[k] = polynomAtempDegree + polynomBtempDegree;
                    }
                    
                    polynomBtempDegree--;
                    k++;
                }
                polynomAtempDegree--;
                polynomBtempDegree = polynomB.PresetDegree;
            }
         
            int maxDegree = (amountOfDegrees.Max() == 0) ? Math.Max(polynomA.PresetDegree, polynomB.PresetDegree) : amountOfDegrees.Max();            
            var reducedPolynomForm = new Polynomial(maxDegree, new int[maxDegree + 1]);
           
            for(var i = 0; i < maxDegree + 1; i++)
            {
                for(var j = 0; j < amountOfDegrees.Length; j++)
                {
                   if(amountOfDegrees[j] == i)
                   {
                        reducedPolynomForm[i] += expandedPolynomForm[j];                         
                   }
                }             
            }
             reducedPolynomForm._coefficients = reducedPolynomForm._coefficients.Reverse().ToArray();

             return reducedPolynomForm;
        }

         public static Polynomial operator+ (Polynomial polynomA, Polynomial polynomB) => Add(polynomA, polynomB);
       
         public static Polynomial operator- (Polynomial polynomA, Polynomial polynomB) => Subtract(polynomA, polynomB);      
              
         public static Polynomial operator- (Polynomial polynom)
        { 
            for(var i = 0; i < polynom.PresetDegree; i++)
            {
                polynom[i] *= -1;
            }
             return polynom;          
        }

        public static Polynomial operator* (Polynomial polynomA, Polynomial polynomB) => Multiply(polynomA, polynomB);     


        public double CalculatePolynomialValue(double variableValue)
        {
            double polynomialValue = 0d;
            int tempDegree = _presetDegree;

            for(var i = 0; i < _coefficients.Length - 1; i++)
            {
                polynomialValue += _coefficients[i] * Math.Pow(variableValue, tempDegree);
                tempDegree--;
            }
            polynomialValue += _coefficients[_coefficients.Length - 1];
       
            return polynomialValue;
        }

        public override string ToString()
        {
            StringBuilder polynomial = new StringBuilder(_coefficients.Length * 3);

            int tempDegree = _presetDegree;
            string sign;

            for (int i = 0; i < _coefficients.Length; i++)
            {
                sign = ((_coefficients[i] > 0) && (polynomial.Length != 0)) ? "+" : "";

                if(i == _coefficients.Length - 1)
                { 
                    sign = ((_coefficients[i] >= 0) && (polynomial.Length != 0)) ? "+" : "";
                    polynomial.Append($"{ sign }{ _coefficients[i] }");
                    break;
                }
              
                if (_coefficients[i] != 0)
                {
                    if ((tempDegree == 0) || (tempDegree == 1))
                    {
                        if (_coefficients[i] == 1)
                        {
                            polynomial.Append($"{ sign }{ _variableName }");
                        }
                         else if (_coefficients[i] == -1)
                        {
                            polynomial.Append($"-{ _variableName }");
                        }
                        else
                        {
                            polynomial.Append($"{ sign }{ _coefficients[i] }{ _variableName }");
                        }
                    }
                    else
                    {
                        if (_coefficients[i] == 1)
                        {
                            polynomial.Append($"{ sign }{ _variableName }^{ tempDegree }");                         
                        }
                        else if (_coefficients[i] == -1)
                        {
                            polynomial.Append($"-{ _variableName }^{ tempDegree }");
                        }
                        else
                        {
                            polynomial.Append($"{ sign }{ _coefficients[i] }{ _variableName }^{ tempDegree }");                           
                        }
                        tempDegree--;
                    }
                } 
                 else
                {
                    tempDegree--;
                }
             }         
            return polynomial.ToString();
        }

        public Polynomial(int degree, char variableName, params int[] coefficients)
        {
            if((degree <= 0) || (coefficients.Length - degree > 1) || (coefficients.Length == 0))
            {              
                throw new ArgumentOutOfRangeException();                          
            }

             _coefficients = new int[degree + 1];

            _presetDegree = degree;
            _highestDegree = degree;

             bool highestDegreeIsFound = false;          

            for(var i = 0; i < _coefficients.Length; i++)
            {
                _coefficients[i] = (i < coefficients.Length) ? coefficients[i] : 0;

                if (!highestDegreeIsFound)
                {
                    if (_coefficients[i] == 0)
                    {
                        _highestDegree--;
                    }
                    else
                    {                      
                        highestDegreeIsFound = true;
                    }
                }
            }       
            _variableName = variableName;         
        }

       
        public Polynomial(int degree, params int[] coefficients) : this(degree, 'x', coefficients)
        {        
        }

    }
}
